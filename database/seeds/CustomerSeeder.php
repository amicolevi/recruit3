<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class CustomerSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('customers')->insert([
           [ 'name' => 'a',
            'email' => 'a.@gmail.com',
            'phone' => '0505050505',
            'user_id'=> 1,
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ],
        [
            'name' => 'b',
            'email' => 'b.@gmail.com',
            'phone' => '0101010101',
            'user_id'=> 1,
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ],
        [
            'name' => 'c',
            'email' => 'c.@gmail.com',
            'phone' => '0202020202',
            'user_id'=> 1,
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ],]
    ); 
    }
}
