<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class CarscategorysSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('carscategory')->insert([
            [
                'category' => 'turist',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ],
            [
                'category' => 'bussines',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ],        
            [
                'category' => 'first class',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ],                
            ]);   
    }
}
