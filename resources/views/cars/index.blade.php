@extends('layouts.app')

@section('title', 'cars')

@section('content')      

<div><a href =  "{{url('/cars/create')}}"> Add new car</a></div>
       
       <h1>Cars list</h1>
        
    <table class = "table table-dark">
    <th>id</th><th>Car Number</th><th>Year</th><th>Created</th><th>Updated</th>
    @foreach($cars as $car)
        <tr>
        <td>{{$car->id}}</td>
        <td>{{$car->car_number}}</td>
        <td>{{$car->year}}</td>

        <td>
                <div class="dropdown">
                    <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        @if(isset($car->category_id))
                          {{$car->categorys->name}}  
                        @else
                          Assign owner
                        @endif
                    </button>
                    <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                    @foreach($categorys as $category)
                      <a class="dropdown-item" href="{{route('cars.changecategory',[$cars->id,$category->id])}}">{{$category->name}}</a>
                    @endforeach
                    </div>
                  </div>                
            </td>



        <td>{{$car->created_at}}</td>
        <td>{{$car->updated_at}}</td>
        </tr>
        @endforeach
    </table>
@endsection
  

        