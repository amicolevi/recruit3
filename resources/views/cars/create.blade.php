@extends('layouts.app')

@section('title', 'Create car')

@section('content')
        <h1>Create car</h1>
        <form method = "post" action = "{{action('CarsController@store')}}">
        @csrf 
        <div class="form-group">
            <label for = "car_number">Car number</label>
            <input type = "number" class="form-control" name = "car_number">
        </div>     
        <div class="form-group">
            <label for = "year">Year</label>
            <input type = "number" class="form-control" name = "year">
        </div> 
        
        <div>
            <input type = "submit" name = "submit" value = "Create car">
        </div>                    
        </form>    
@endsection
