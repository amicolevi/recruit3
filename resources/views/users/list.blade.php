@extends('layouts.app')

@section('title', 'users')

@section('content')      

       <h1>users information</h1>
        
    <table class = "table table-dark">
    <th>id</th><th>Name</th><th>Email</th><th>Department</th><th>Created</th><th>Updated</th>
@foreach($users as $user)
        <tr>
        <td>{{$user->id}}</td>
        <td>{{$user->name}}</td>
        <td>{{$user->email}}</td>
        <td>{{$user->department->name}}</td>
        <td>{{$user->created_at}}</td>
        <td>{{$user->updated_at}}</td>
        <td>
                <a href = "{{route('users.edit',$user->id)}}">Edit</a>
            </td> 
        <td>
            <a href = "{{route('users.delete',$user->id)}}">Delete</a>
        </td> 

        </tr>
 @endforeach
    </table>
@endsection
  

        