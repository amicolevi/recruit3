@extends('layouts.app')

@section('title', 'Edit user')

@section('content')       
       <h1>Edit user</h1>
        <form method = "post" action = "{{action('UsersController@update',$user->id)}}">
        @csrf
        @METHOD('PATCH')
        <div class="form-group">
            <label for = "name">User name</label>
            <input type = "text" class="form-control" name = "name" value = {{$user->name}}>
        </div>     
        <div class="form-group">
            <label for = "email">User email</label>
            <input type = "text" class="form-control" name = "email" value = {{$user->email}}>
        </div> 
        <div class="form-group">
            <label for = "name">User password</label>
            <input type = "number" class="form-control" name = "password" value = {{$user->password}}>
        </div> 
        <div class="form-group row">
                            <label for="department_id" class="col-md-4 col-form-label text-md-right">Department</label>
                            <div class="col-md-6">
                                <select class="form-control" name="department_id">                                                                         
                                   @foreach ($departments as $department)
                                     <option value="{{ $department->id }}"> 
                                         {{ $department->name }} 
                                     </option>
                                   @endforeach    
                                 </select>
                            </div>
        <div>
            <input type = "submit" name = "submit" value = "Update candidate">
        </div>                       
        </form>    
    </body>
</html>
@endsection
