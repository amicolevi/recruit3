@extends('layouts.app')

@section('title', 'statuses information')

@section('content')      

       <h1>Statuses information</h1>
        
    <table class = "table table-dark">
    <th>id</th><th>Name</th><th>Next stage1</th><th>Next stage2</th>
    @foreach($statuses as $status)
    <tr style="background-color:{{App\Status::next($status->id)  != null ? 'lightgreen':''}}">
        <td>{{$status->id}}</td>
        <td>{{$status->name}}</td>
        
        @foreach((App\Status::next($status->id)) as $sta)
        <td>
                     {{$sta->name}}
                @endforeach   
                </td> 
        @endforeach
    </table>
    
@endsection
  

        