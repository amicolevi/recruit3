@extends('layouts.app')

@section('title', 'Candidate')

@section('content')
@if(Session::has('notallowed'))
<div class = 'alert alert-danger'>
    {{Session::get('notallowed')}}
</div>
@endif
<h1>Candidate details</h1>
<table class = "table table-dark">
    <!-- the table data -->
        <tr>
            <td>Id</td><td>{{$candidate->id}}</td>
        </tr>
        <tr>
            <td>Name</td><td>{{$candidate->name}}</td>
        </tr>
        <tr>
            <td>Email</td><td>{{$candidate->email}}</td>
        </tr> 
        <tr>   
            <td>Age</td><td>{{$candidate->age}}</td>
        </tr> 
        <tr>
        <td>Owner</td>
        <td>  
            @if(isset($candidate->owner))
              {{$candidate->owner->name}}
            @else
              No owner assigned yet   
            @endif  
        <td>
        </tr>
        <tr>
           <td>Status</td> <td>{{$candidate->status->name}}</td>    
        </tr>    
        <tr>
           <td>Created</td><td>{{$candidate->created_at}}</td>
        </tr>
        <tr>
           <td>Updated</td><td>{{$candidate->updated_at}}</td>  
        </tr>    
        </table>
                                                        

                <form method="POST" action="{{ route('candidates.changestatusfromcandidate') }}">
                    @csrf  
                    <div class="form-group row">
                    <label for="department_id" class="col-md-4 col-form-label text-md-right">Move to status</label>
                    <div class="col-md-6">
                        <select class="form-control" name="status_id">                                                                         
                          @foreach (App\Status::next($candidate->status_id) as $status)
                          <option value="{{ $status->id }}"> 
                              {{ $status->name }} 
                          </option>
                          @endforeach    
                        </select>
                    </div>
                    <input name="id" type="hidden" value = {{$candidate->id}} >
                    <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    Change status
                                </button>
                            </div>
                    </div>                    
                </form> 
                </div>

@endsection
