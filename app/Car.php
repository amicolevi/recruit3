<?php

namespace App;

use Illuminate\Support\Enumerable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Car extends Model
{
    protected $fillable = ['car_number','year'];

    public function categorys(){
        return $this->belongsTo('App\Providers\Category');
    }
}
