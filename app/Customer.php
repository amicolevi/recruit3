<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{
    protected $fillable = ['name','email', 'phone'];
    
     public function saler(){
        return $this->belongsTo('App\User','user_id');
    }
}
