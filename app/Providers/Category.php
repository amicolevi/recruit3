<?php

namespace App\Providers;

use Illuminate\Support\Enumerable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Car extends Model
{
    public function cars()
    {
        return $this->hasMany('App\Car');
    } 
}
